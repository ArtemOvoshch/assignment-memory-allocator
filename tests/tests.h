#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include "../include/mem_internals.h"
#include "../include/mem.h"
#include "../include/util.h"
#include <stdlib.h>

void all_tests(void);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
