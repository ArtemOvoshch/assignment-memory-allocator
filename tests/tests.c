#include "tests.h"

//I hope that I won't get bonked for the arrays and variables for testing memory they are using:))) I can rewrite it for non-connected one but with raw checking of memory allocation(no other usage of variables)

static void* test_init(size_t capacity){
    printf("------------------\n    First initial:\n");
    void* heap = heap_init(capacity);

    if(heap==NULL){
        printf("Initial failed.\n");
        exit(0);
    }

    debug_heap(stdout, heap);
    printf("Test initial passed\n");
    return heap;
}

static void test_allocate(size_t count, size_t numbers[], void** pointer, void* heap){
    printf("------------------\n    Allocate test:\n\n");

    for(size_t i = 0; i < count; i++){
        pointer[i] = _malloc(numbers[i]);
        if((block_get_header(pointer[i]))->capacity.bytes == size_max(numbers[i], BLOCK_MIN_CAPACITY)){
            printf("Was able %zu\n", numbers[i]);
        }
        else{
            printf("Test allocated failed for %zu\n", numbers[i]);
            exit(0);
        }
    }

    debug_heap(stdout, heap);
    printf("\n  Test Allocate passed\n");
}

static void test_free_one(size_t number_for_freeing, void** pointer, void* heap){
    printf("------------------\n    Freeing one test:\n\n");
    void* current = pointer[number_for_freeing];
    _free(current);

    if(block_get_header(current)->is_free){
        printf("Was able %zu\n", block_get_header(current)->capacity.bytes);
    }
    else{
        printf("Test freeing one failed for %zu\n", block_get_header(current)->capacity.bytes);
        exit(0);
    }

    debug_heap(stdout, heap);
    printf("\n  Test Freeing one passed\n");
}

static void test_free_two(size_t count, size_t* blocks, void** pointer, void* heap){
    printf("------------------\n    Freeing two test:\n\n");

    for(size_t i = 0 ; i < count; i++){
        void* current = (struct block_header*)pointer[blocks[i]];
        _free(current);
        if(block_get_header(current)->is_free){
            printf("Was able for %zu\n", block_get_header(current)->capacity.bytes);
        }
        else{
            printf("Test freeing two failed for %zu\n", block_get_header(current)->capacity.bytes);
            exit(0);
        }
    }

    debug_heap(stdout, heap);
    printf("\n  Test Freeing two passed\n");
}


//show how malloc can add new memory to the new heap. Also can show how algorithms merge free blocks (compare heap_debug from previous test and current one)
static void test_heap_usage(size_t heap_size, void* heap){
    printf("------------------\n    Heap extension test:\n\n");
    size_t divided_size = heap_size / 2;
    struct block_header* first_block = _malloc(divided_size);
    struct block_header* second_block = _malloc(heap_size);
    bool result = (first_block!=NULL)&&(second_block!=NULL)&&(second_block->is_free==false)&&(first_block->is_free==false);

    if(!result){
        printf("Test about raising the heap failed for heap extension failed\n");
        exit(0);
    }

    bool capacity_result = (block_get_header(first_block)->capacity.bytes == divided_size)&&(block_get_header(second_block)->capacity.bytes == heap_size);

    if(!capacity_result){
        printf("Test about raising the heap failed for capacity failed\n");
        exit(0);
    }

    debug_heap(stdout, heap);
    printf("\n  Heap usage test passed\n");
}

static void test_heap_allocated_far_away(void* heap, size_t heap_size){
    printf("------------------\n    Heap extension far away test:\n\n");
    struct block_header* last = (struct block_header*) heap;
    debug_heap(stdout, heap);

    while(last->next) last = last->next;

    void* empty_address = last->contents+last->capacity.bytes;
    void* allocated_memory = _malloc(heap_size*4);

    if(allocated_memory==empty_address){
        printf("\n  Test heap extension far away failed\n");
        debug_heap(stdout, heap);
        exit(0);
    }

    debug_heap(stdout, heap);
    printf("\n Test heap extension far away passed\n");
}

void all_tests(void){
    //init testing parameters
    size_t heap_size = 10000;
    size_t numbers[] = {20, 200, 400, 1200, 5000};
    size_t count = sizeof(numbers)/sizeof(size_t);
    void* pointers[count];
    size_t numbers_of_blocks[] = {3,4};
    size_t count_blocks = sizeof(numbers_of_blocks)/sizeof(size_t);
    //init heap test
    void* heap = test_init(heap_size);
    //test that allocate memory
    test_allocate(count, numbers, pointers, heap);
    //test that free one block
    test_free_one(3, pointers, heap);
    //test that free two blocks
    test_free_two(count_blocks, numbers_of_blocks, pointers, heap );
    //test that extends heap
    test_heap_usage(heap_size, heap);
    //to be honest: this one have no practical difference with the previous one
    test_heap_allocated_far_away(heap, heap_size);
}
