#define _DEFAULT_SOURCE


#include <unistd.h>
#include <stddef.h>
#include <sys/mman.h>
#include "../include/mem_internals.h"
#include "../include/mem.h"
#include "../include/util.h"



void debug_block(struct block_header* b, const char* fmt, ... );

void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );

extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough( struct block_header* block, size_t query ) { return block->capacity.bytes >= query; }

static size_t   pages_count   ( size_t mem ) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t   round_pages   ( size_t mem ) { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

// try to allocate some region of the memory and
static struct region alloc_region  ( void const * addr, size_t query ) {
    if(!addr) return REGION_INVALID;

    size_t size_of_query = region_actual_size(query);
    //try to create region
    struct region reg = (struct region){
        .addr = map_pages(addr, size_of_query, MAP_FIXED_NOREPLACE),
        .size = size_of_query,
        .extends = true
    };
    // instruction to the failure of the previous point
    if(reg.addr == MAP_FAILED){
        reg.addr = map_pages(addr, size_of_query, 0);
        if(reg.addr == MAP_FAILED){
            return REGION_INVALID;
        }
        reg.extends = false;
    }
    //initialize block
    block_init((void*)addr, (block_size) {.bytes = reg.size}, NULL);

    return reg;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}


static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//split block if it is too big so that it was enough for our needs and the free divided was not too small
static bool split_if_too_big( struct block_header* block, size_t query ) {
    query = size_max(query, 24);
    //create part of the block as another one if our is too big
    if(block_splittable(block, query)&&(block)){
        struct block_header* second = (struct block_header*)((block->contents)+query);
        const block_size second_size = {.bytes = block->capacity.bytes - query};
        block_init((void*)second, second_size, block->next);
        block->next = (struct block_header*)second;
        block->capacity.bytes = block->capacity.bytes - second_size.bytes;
        return true;
    }

    return false;
}



static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

//merge our block with the next if it is free
static bool try_merge_with_next( struct block_header* block ) {
    if(block){
        if(!block->next){
            return false;
        }
    }

    if(mergeable(block, block->next)){
        block->capacity.bytes = block->capacity.bytes + block->next->capacity.bytes + offsetof(struct block_header, contents);
        block->next = block->next->next;
        return true;
    }

    return false;
}


/*  if size of the heap is enough  */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

//function to merge all free continuous blocks
static void merge_all(struct block_header* block){
    while(try_merge_with_next(block));
}

//try to find block with bigger size in compare to the required one
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if(!block) return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};

    struct block_search_result bsr = {.type = BSR_REACHED_END_NOT_FOUND, .block = NULL};
    struct block_header* next_search = block;
    struct block_header* last = NULL;

    while(next_search){
        if(next_search->is_free) {
            merge_all(next_search);
            if (block_is_big_enough(next_search, sz)) {
                split_if_too_big(next_search, sz);
                bsr.type = BSR_FOUND_GOOD_BLOCK;
                bsr.block = next_search;
                return bsr;
            }
        }
        if(!next_search->next){
            last = next_search;
        }
        next_search = next_search->next;
    }

    bsr.block = last;
    return bsr;
}

//try to allocate memory without the extension of the heap
static struct block_search_result try_memalloc_existing (struct block_header* block, size_t query ) {
    return find_good_or_last(block, query);
}


//extend heap
struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if(!last) return NULL;

    const struct region new_region = alloc_region(block_after(last), query);

    if(region_is_invalid(&new_region)){
        return NULL;
    }

    last -> next = new_region.addr;
    return new_region.addr;
}

//malloc but mini-version
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if(!heap_start){
        return NULL;
    }

    struct block_search_result found_memory = try_memalloc_existing(heap_start, query);

    if(found_memory.type == BSR_REACHED_END_NOT_FOUND){
        struct block_header* found = grow_heap(found_memory.block, query );
        found_memory.block = (try_memalloc_existing(found,query)).block;
    }

    return found_memory.block;
}

//gigachad
void* _malloc( size_t query ){
    size_t max_query = size_max(query, BLOCK_MIN_CAPACITY);

  struct block_header* const addr = memalloc( max_query, (struct block_header*) HEAP_START );

  if (addr) {
      addr->is_free = false;
      return (void*)(addr->contents);
  }

  return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

//free the memory
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  merge_all(header);
}
